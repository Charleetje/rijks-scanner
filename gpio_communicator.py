import RPi.GPIO as GPIO

DOOR_SENSOR_PIN = 12
DOOR_LOCK_PIN = 11
DRAWER_LOCK_PIN = 13

RFID_PRESENT_PIN = 15
RFID_CORRECT_PIN = 16

CLOSE_MAGNET_SIGNAL = GPIO.LOW # to be defined
OPEN_MAGNET_SIGNAL = GPIO.HIGH # to be defined
CLOSE_MAGNET_SIGNAL = GPIO.HIGH # to be defined
OPEN_MAGNET_SIGNAL = GPIO.LOW # to be defined

class GPIOCommunicator(object):

    def setup_pins(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(DOOR_SENSOR_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(RFID_PRESENT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(RFID_CORRECT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(DOOR_LOCK_PIN, GPIO.OUT, initial=OPEN_MAGNET_SIGNAL)
        GPIO.setup(DRAWER_LOCK_PIN, GPIO.OUT, initial=CLOSE_MAGNET_SIGNAL)

        self.last_closed = GPIO.input(DOOR_SENSOR_PIN)
        print("Door state on startup: ", self.last_closed)

    def set_door_state_callback(self, callback):
        GPIO.add_event_detect(DOOR_SENSOR_PIN, GPIO.RISING, callback=callback, bouncetime=500)

    def is_rfid_present(self):
        return GPIO.input(RFID_PRESENT_PIN)
        
    def is_rfid_correct(self):
        return GPIO.input(RFID_CORRECT_PIN)

    def is_door_closed(self):
        door_status = GPIO.input(DOOR_SENSOR_PIN)
        print("Is door closed? ", door_status)
        return door_status

    def door_state_change(self):

        currently_closed = GPIO.input(DOOR_SENSOR_PIN)
        print("Current state of door: ", currently_closed)
        if currently_closed is not self.last_closed:
            self.last_closed = currently_closed
            return (True, self.last_closed)
        else:
            return (False, self.last_closed)

    def lock_door(self):
        print("Locking door")
        GPIO.output(DOOR_LOCK_PIN, CLOSE_MAGNET_SIGNAL)

    def release_door(self):
        print("Open door")
        GPIO.output(DOOR_LOCK_PIN, OPEN_MAGNET_SIGNAL)

    def lock_drawer(self):
        print("Locking drawer")
        GPIO.output(DRAWER_LOCK_PIN, CLOSE_MAGNET_SIGNAL)

    def release_drawer(self):
        print("Open drawer")
        GPIO.output(DRAWER_LOCK_PIN, OPEN_MAGNET_SIGNAL)