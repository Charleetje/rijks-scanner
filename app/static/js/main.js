var PATHOGEN_ANSWER = "E. Coli"
var ANTIBIOTIC_ANSWER = "Penicillin"

var loadSpeed = 500;
var debugVideo = false;
var alwaysRight = true;
var slowProcessor = false;

var toFocus = [$('#input-pathogen'), $('#input-antibiotic')]
var focusIndex = 0;

var gameState = "waitingForSample"

// Will execute myCallback every 0.5 seconds 
var intervalID = window.setInterval(toggleHighlight, 1500);

fillLoadbarCallback = null;

if (debugVideo)
{
  clearInterval(intervalID);
  loadVideo();
}

var loadCounter = 0;

var visToggler = false;

function changeVisibility(id, vis_val)
{
  console.log("Changing visibility", id, vis_val);
 if (!slowProcessor)
 {
  $(id).toggleClass('no-opacity');
 }
 else
 {
  var dis_val;
  if (!vis_val)
  {
   dis_val = 'none';
  }
  else
  {
   dis_val = 'flex';
  }
  $(id).css('display',dis_val );
 }
}

function toggleHighlight() {
  changeVisibility('#texter', visToggler);
  visToggler = !visToggler;
}

function processSampleInput() {
  if (gameState === "waitingForSample")
  {
    gameState = "processing"
    console.log("Waiting clicked");
    console.log(gameState);
    clearInterval(intervalID);
    changeVisibility('#texter', false);
    window.setTimeout(function(){
      $('#text').html('<h3>Processing..</h3>');
      $('#loadbar').css('display', 'flex');
      changeVisibility('#texter', true);
      fillLoadbarCallback = loadVideo;
      window.setTimeout( fillLoadbar, 3000);
      }, 1500);
  }
}

$('#texter').click(function(){
  processSampleInput();
});

function fillLoadbar(callback)
{
  loadCounter++;
  console.log(loadCounter);
  if (loadCounter <= 25)
  {
    $('#loadbar').append("<div class='cal-day highlighted'></div>")
    window.setTimeout( fillLoadbar,
                               Math.floor(Math.random()*loadSpeed));
  }
  else
  {
    if (fillLoadbarCallback)
    {
      fillLoadbarCallback();
      fillLoadbarCallback = null;
      loadCounter = 0;
    }
    else
    {
      console.log("End of loading reached, but no callback set");
    }
  }
}

function loadVideo()
{
  $('#texter').addClass('no-opacity');
  window.setTimeout(function(){
    $('#texter').css('display', 'none');
    $('#videobox').css('display', 'flex');
    toFocus[focusIndex].focus();
    var vid = document.getElementById("video"); 
    vid.play();
  }, 1000);
  gameState = "patientInput";
}

function checkKey(e)
{
  e = e || window.event;
  console.log(e.keyCode);
  if (e.keyCode == 40)
  {
    focusIndex = (focusIndex + 1) % toFocus.length;
    toFocus[focusIndex].focus();
  }
  else if (e.keyCode == 38)
  {
    focusIndex = (focusIndex + toFocus.length - 1) % toFocus.length;
    toFocus[focusIndex].focus();
  }
  else if (e.keyCode == 13 && gameState === "patientInput")
  {
    verifyInput();
  }
  else if (e.keyCode == 13 && gameState === "alarmMessage")
  {
    generateTestCultures();
  }
}
document.onkeydown = checkKey;

function verifyInput() {
  
  if ((elementAnsweredCorrect('#input-pathogen', PATHOGEN_ANSWER) && elementAnsweredCorrect('#input-antibiotic', ANTIBIOTIC_ANSWER)) || alwaysRight )
  {
    testScreen();
  }
}

function elementAnsweredCorrect(box_id, solution) {
  var box = $(box_id);
  if (box.val() === solution)
  {
    return true;
  }
  else
  {
    box.val(''); // empty box
    return false;
  }
};

function alarmMessage() {
  gameState = "alarmMessage"
  $('#loadbar').css('display', 'none');
  $('#text').html('<h4 style="color: red; text-shadow: 2px 2px 2px #FFFF00;">Antibiotic resistance detected</h4><p>Additionally there is a high risk for blood infection.</p><p>Please inform the patient.</p><p>We need to find an alternative antibioticum that can be used to fight the bacteria.</p><input class="highlighted" type="button" value="Generate testing cultures" style="width: auto" />');
}

function testScreen() {
  $('#videobox').css('display', 'none'); 
  $('#text').html('<h3>Testing..</h3>');
  $('#loadbar').html('');
  $('#loadbar').css('display', 'flex');
  $('.textbox').removeClass('no-opacity');
  $('#texter').css('display', 'flex');
  fillLoadbarCallback = alarmMessage;
  window.setTimeout( fillLoadbar, 1000);
}


function generateTestCultures() {
  $('.textbox').addClass('no-opacity');
  window.setTimeout(function(){
    $('#text').html('<h3>Generating test cultures</h3>');
    $('#loadbar').css('display', 'flex');
    $('.textbox').removeClass('no-opacity');
    fillLoadbarCallback = openDrawer;
    $('#loadbar').html('');
    window.setTimeout( fillLoadbar, 1000);
    }, 2000);
}

function openDrawer()
{
  $.post("/", {});
}

// Establish socket connection so browser/javascript can receive messages from server/flask
var socket = io.connect('http://' + document.domain + ':' + location.port);

socket.on('new_id', function(msg) {
  console.log( msg.id);
  socket.emit('remove_rfid_reading')
  processSampleInput();
})