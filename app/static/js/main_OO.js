const PATHOGEN_ANSWER = ["E. coli", "E. Coli", "e. coli", "E.Coli", "E.coli", "ecoli", "e coli", "E coli", "E Coli", "Escherichia Coli", "Escherichia coli", "escherichia coli"];
const ANTIBIOTIC_ANSWER = ["Amoxicillin", "amoxicillin", "Amoxicilline", "amoxicilline", ];

const NUMBER_OFFSET = 49; // Offset for KeyCode of 1 (keyCOde for 1 is 49, for 2 is 50 etc.)

const WRONG_INPUT_TEXT = "De gegeven input is niet correct";

secret_code_follower = { "!" : false, "@" : false, "#" : false, "$": false, "%" : false, "^" : false, "PageDown" : false, "PageUp" : false, "End" : false};
secret_code_callbacks = [ [["!", "#", "%"], function(){ if(window.last_audio) {last_audio.pause();} clear_all_timeouts(); load_next_state();} ],
                          [["!", "%", "^"], function(){ if(window.last_audio) {last_audio.pause();} clear_all_timeouts(); load_previous_state(); }],
                          [["#", "%", "^"], function(){ override_correct_rfid = true; }],
                          [["@", "#", "%"], function() { socket.off('no_rfid_read'); window.noRFIDremoveaction = true; }],
                          [["PageDown", "End"], function() { $('#input-pathogen').val(PATHOGEN_ANSWER[0]); $('#input-antibiotic').val(ANTIBIOTIC_ANSWER[0]); $('#input-antibiotic').focus(); } ] ];


loadSpeed = 19000;
loadBlocks = 30;
loadBarWait = 0;

noRFIDremoveaction = false;

timeout_list = [];

function clear_all_timeouts()
{
  for (var i=0; i < timeout_list.length; i++)
  {
    clearTimeout(timeout_list[i]);
  }
  timeout_list = [];
}

$(document).ready(function() {
  var protocol = window.location.protocol;
  window.socket = io.connect(protocol + '//' + document.domain + ':' + location.port);

  gamestates = []
  gamestates.push(new GameState('0_wrongsample', 'body',
                                [function(){play_audio('UI_answer_wrong');}, open_door, setup_listen_remove_rfid],
                                []));
  gamestates.push(new GameState('05_welcome', 'body',
                                [reset_global_variables, open_door_and_close_drawer, setup_keyboard_listeners_return_load_next],
                                [remove_return_functionality]));
  gamestates.push(new GameState('1_samplewaiting', 'body',
                                [setup_listen_rfid],
                                []));
  gamestates.push(new GameState('2_doorclosewaiting', 'body',
                                [setup_listen_doorclose],
                                [remove_listen_rfid,remove_listen_doorclose]));
  gamestates.push(new GameState('3_firstprocessing', 'body',
                                [check_rfid],
                                []));
  gamestates.push(new GameState('3_firstprocessing2', 'body',
                                [function(){play_audio('UI_answer_right');}, setup_keyboard_listeners_return_load_next],
                                []));
  gamestates.push(new GameState('4_inputscreen', 'body',
                                [ setup_keyboard_listeners_inputscreen, focus_on_input_field, setArrowUpDownToSwitchInput],
                                [remove_return_functionality, cancel_setArrowUpDownToSwitchInput]));
  gamestates.push(new GameState('5_secondprocessing', 'body',
                                [second_processing_animation],
                                []));
  gamestates.push(new GameState('6_resistancewarning1', 'body',
                                [setup_keyboard_listeners_return_load_next],
                                []));
  gamestates.push(new GameState('6_resistancewarning2', 'body',
                                [setup_keyboard_listeners_return_load_next],
                                []));
  gamestates.push(new GameState('7_generatetestcultures0', 'body',
                                [setup_keyboard_listeners_return_load_next],
                                []));
  gamestates.push(new GameState('7_generatetestcultures1', 'body',
                                [generating_testcultures],
                                []));
  gamestates.push(new GameState('8_bacteriaisolated', 'body',
                                [setup_keyboard_listeners_return_load_next],
                                []));
  gamestates.push(new GameState('9_antibioticatest', 'body',
                                [setup_keyboard_listeners_return_load_next, focus_on_antibiotic, setArrowRightLeftToSelect, setArrowUpDownToSelectRow],
                                [cancel_setArrowRightLeftToSelect, cancel_setArrowUpDownToSwitchInput]));
  gamestates.push(new GameState('10_confirmantibiotica', 'body',
                                [load_chosen_antibiotic, setup_keyboard_listeners_previous_or_next, focus_on_answer, setArrowRightLeftToSelect],
                                [cancel_setArrowRightLeftToSelect]));
  gamestates.push(new GameState('11_communicate_antibiotica', 'body',
                                [],
                                []));

  let searchParams = new URLSearchParams(window.location.search);
  if (searchParams.has('page'))
  {
    let page = searchParams.get('page');
    load_state(page);
  }
  else
  {
    load_state(1);
  }

  document.addEventListener("keydown", checkKeyDown, false);
  document.addEventListener("keyup", checkKeyUp, false);
  document.addEventListener("keypress", ignoreKeysTextbox, false);
});

///////////////////////////////////////////////////////////////////////////////////////
///// DEFINITION OF FUNCTIONS USED ABOVE //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

function ignoreKeysTextbox(e)
{
  return false;
}

function reset_global_variables()
{
  last_audio = null;
  check_rfid = false;
  override_correct_rfid = false;
  clear_all_timeouts();
}

function GameState(name, html_load_id, entry_actions, exit_actions)
{
  this.name = name;
  this.html_load_id = html_load_id;
  this.entry_actions = entry_actions;
  this.exit_actions = exit_actions;
}

function load(object, my_args)
{
  console.log(object.name, " load");
  // Fill html tag with html returned through "get request"
  $.get('/', { 'name' : object.name }, function(data) {
      console.log("data", data);
      console.log(my_args);
      $(object.html_load_id).html(data);
      for (var i = 0; i < object.entry_actions.length; i++) {
      console.log("Performing entry action", i);
      try { arg = my_args[i]; } catch { arg = undefined; }
      object.entry_actions[i](object, arg);
    }
  });
};

function exit(object)
{
  if( window.last_audio)
  {
    last_audio.pause();
  }
  for (var i = 0; i < object.exit_actions.length; i++) {
    object.exit_actions[i]();
  }
}

function load_state(index, my_args)
{
  console.log("Loading game state", index, gamestates[index].name, "with arguments", my_args);
  load(gamestates[index], my_args);
  current_state = index;
}

function load_next_state(my_args)
{
  console.log("Arguments in load_next_state", my_args);
  exit(gamestates[current_state]);
  load_state(current_state + 1, my_args);
}

function load_previous_state(my_args)
{
  exit(gamestates[current_state]);
  load_state(current_state - 1);
}

function open_door()
{
  console.log("Opening sample door");
  socket.emit('release_door');
}

function open_door_and_close_drawer()
{
  open_door();
  console.log("Locking sample drawer");
  socket.emit('lock_drawer');
}

function setup_listen_remove_rfid()
{
    socket.on('no_rfid_read', function(msg)
    {
      console.log('No RFID in sample_reader');
      if (!window.noRFIDremoveaction)
      {
        exit(gamestates[current_state]);
        load_state(current_state + 2);
      }
      socket.off('no_rfid_read');
    });
}

function setup_listen_rfid()
{
  socket.on('rfid_read', function(msg)
  {
    console.log('RFID received');
    console.log('Message', msg);
    window.correct_rfid = msg.solved;
    if (gamestates[current_state].name === '1_samplewaiting') // only go to next state if waiting for RFID (but allow "correcting" sample later)
    {
        load_next_state();
    }
  });
}

function load_chosen_antibiotic()
{
  console.log(window.toFocus[window.focusIndex].attr('src'));
  $('#chosen_antibiotic').attr('src', window.toFocus[window.focusIndex].attr('src'));
}

function setup_listen_doorclose()
{
  socket.on('door_state_change', function(msg)
  {
    console.log("Door closed")
    load_next_state();
  });
  socket.emit('allow_door_close');
}

function remove_listen_rfid()
{
  socket.emit('remove_rfid_reading');
  socket.off('rfid_read');
}

function remove_listen_doorclose()
{
  socket.off('door_state_change');
}

function check_rfid(object, my_args)
{
    console.log("check_rfid", window.correct_rfid, "override_correct_rfid", window.override_correct_rfid);
    if (window.correct_rfid || window.override_correct_rfid)
    {
      fillLoadbarCallback = function() {
        // $('#container').html('<div style="width: 800px; height: 400px; background-color: green;"></div>');
        timeout_list.push(window.setTimeout(load_next_state, 500));
      };
    }
    else
    {
      fillLoadbarCallback = function() {
        play_audio('UI_answer_wrong');
        // $('#container').html('<div style="width: 800px; height: 400px; background-color: red;"></div>');
        timeout_list.push(window.setTimeout(function() {
           exit(gamestates[current_state]);
           load_state(current_state - 4);
        }, 200));
      };
    }
    play_audio('UI_scan_sound_starts_earlier');

    fillLoadbar();
}

function second_processing_animation()
{
  window.fillLoadbarCallback = load_next_state
  window.loadSpeed = 150;
  fillLoadbar();
}

function generating_testcultures()
{
  document.getElementById('samples').addEventListener('ended', function() {
      play_audio('UI_finished');
      load_next_state();
      console.log("Opening drawer");
      socket.emit('release_drawer');
  }, false);
}

function setArrowRightLeftToSelect()
{
  window.arrowLeftAction = function()
  {
    toFocus[focusIndex].removeClass('selected');
    focusIndex = (focusIndex + toFocus.length - 1) % toFocus.length;
    toFocus[focusIndex].addClass('selected');
  }
  window.arrowRightAction = function()
  {
    toFocus[focusIndex].removeClass('selected');
    focusIndex = (focusIndex + 1) % toFocus.length;
    toFocus[focusIndex].addClass('selected');
  }
}

function setArrowUpDownToSelectRow()
{
  window.arrowUpAction = function()
  {
    toFocus[focusIndex].removeClass('selected');
    focusIndex = (focusIndex + 4) % toFocus.length;
    toFocus[focusIndex].addClass('selected');
  }
  window.arrowDownAction = function()
  {
    toFocus[focusIndex].removeClass('selected');
    focusIndex = (focusIndex + 5) % toFocus.length;
    toFocus[focusIndex].addClass('selected');
  }
}


function cancel_setArrowRightLeftToSelect()
{
  window.arrowLeftAction = null;
  window.arrowRightAction = null;
}

function setArrowUpDownToSwitchInput()
{
  window.arrowDownAction = function()
  {
    focusIndex = (focusIndex + 1) % toFocus.length;
    toFocus[focusIndex].focus();
  }
  window.arrowUpAction = function()
  {
    focusIndex = (focusIndex + toFocus.length - 1) % toFocus.length;
    toFocus[focusIndex].focus();
  }
}

function cancel_setArrowUpDownToSwitchInput()
{
  window.arrowDownAction = null;
  window.arrowUpAction = null;
}

function checkKeyDown(e)
{
  e = e || window.event;

  if (e.key == "ArrowDown") // Right arrow for switching input fields
  {
    window.arrowDownAction();
  }
  else if (e.key == "ArrowUp") // Left arrow for switching input fields
  {
    window.arrowUpAction();
  }
  else if (e.key == "ArrowLeft")
  {
    window.arrowLeftAction();
  }
  else if (e.key == "ArrowRight")
  {
    window.arrowRightAction();
  }
  else if (e.keyCode == 13)
  {
    checkKeyReturnAction();
  }
  // Some special hack to alow secret number codes to override things in the program
  else if (e.key in secret_code_follower)
  {
    secret_code_follower[e.key] = true;
  }

  console.log(e.keyCode, e.key, secret_code_follower);
  execute_secret_codes(secret_code_follower, secret_code_callbacks);
}

function is_callback_code_pressed(callback_code, follower)
{

  // See if all characters of the callback code are pressed
  for (var j = 0; j < callback_code.length; j++)
  {
    if (follower[callback_code[j]] === false)
    {
      // The character at the j'th spot of the callback "code" has not been pressed in
      console.log("Key", callback_code[j], "not pressed for", callback_code);
      return false;
    }
    else if (follower[callback_code[j]] == undefined)
    {
      console.log("Callback code registered with character that does not exist in code follower", callback_code[j], follower);
      return false;
    }
  }
  return true;
}

function execute_secret_codes(follower, callbacks)
{
  for (var i = 0; i < callbacks.length; i++)
  {
    var callback_code = callbacks[i][0];

    if (is_callback_code_pressed(callback_code, follower))
    {
      var callback_function = callbacks[i][1];

      console.log("Calling callback for code:", callback_code);

      // reset follower to prevent multiple calls
      for (var j = 0; j < callback_code.length; j++)
      {
        follower[callback_code[j]] = false;
      }

      // check if other button are not also pressed
      for (key in follower)
      {
        if (follower[key])
        {
          console.log("OTHER KEYS ARE ALSO PRESSED");
          return;
        }
      }
      callback_function();
    }
  }
}

function checkKeyUp(e)
{
  // Remove key from tracked keys because key was released
  if (e.key in secret_code_follower)
  {
    secret_code_follower[e.key] = false;
  }
}


function setup_keyboard_listeners_return_load_next()
{
  window.checkKeyReturnAction = function() {
    load_next_state();
    window.checkKeyReturnAction = null;
  }
}

function setup_keyboard_listeners_inputscreen()
{
  window.checkKeyReturnAction = verifyInput;
}

function setup_keyboard_listeners_previous_or_next()
{
  window.checkKeyReturnAction = function() {
    if (window.toFocus[window.focusIndex].html() === 'JA')
    {
      load_next_state();
    }
    else
    {
      load_previous_state();
    }
  }
}


function focus_on_input_field()
{
  window.toFocus = [$('#input-pathogen'), $('#input-antibiotic')]
  window.focusIndex = 0;
  window.toFocus[window.focusIndex].focus();
}

function focus_on_antibiotic()
{
  console.log("Focussing on antibiotic");
  window.toFocus = [$('#antibiotic1'), $('#antibiotic2'), $('#antibiotic3'), $('#antibiotic4'), $('#antibiotic5'), $('#antibiotic6'), $('#antibiotic7'), $('#antibiotic8'), $('#antibiotic9')]
  window.focusIndex = 0;
  window.toFocus[window.focusIndex].addClass('selected');
}

function focus_on_answer()
{
  window.toFocus = [$('#ja'), $('#nee')];
  window.focusIndex = 0;
  window.toFocus[window.focusIndex].addClass('selected');
}

function remove_return_functionality()
{
  window.checkKeyReturnAction = null
}

function play_audio(which_file)
{
  console.log("Playing audio", which_file);
  // var media = document.getElementById(which_file);
  window.last_audio = new Audio( '/static/media/' + which_file + '.mp3');
  const playPromise = last_audio.play();
  if (playPromise !== null){
    playPromise.catch(() => {
      console.log("Error caught");
      last_audio.play()
      .catch(function() {console.log("Errored again");}); });}
}

function verifyInput()
{
  var antibiotic_right = elementAnsweredCorrect('#input-antibiotic', ANTIBIOTIC_ANSWER);
  var pathogen_right = elementAnsweredCorrect('#input-pathogen', PATHOGEN_ANSWER);
  if (pathogen_right && antibiotic_right)
  {
    load_next_state();
  }
  else
  {
    $('#inputdescriber').html(WRONG_INPUT_TEXT);
    $('#inputdescriber').css("color", "red");
  }
}

function elementAnsweredCorrect(box_id, solutions) {
  var box = $(box_id);
  var player_answer = box.val()
  var answered_correctly = false;
  for (var i=0; i<solutions.length; i++)
  {
    correct_answer = solutions[i];
    console.log(correct_answer, player_answer, correct_answer === player_answer);
    if (correct_answer === player_answer)
    {
        answered_correctly = true;
        break;
    }
  }

  if (! answered_correctly)
  {
      box.val('');
      box.focus();
      return false;
  }
  else
  {
      return true;
  }
};

function fillLoadbar()
{
  window.loadCounter = 0;
  _fillLoadbar();
}

function _fillLoadbar()
{
  loadCounter++;
  console.log(loadCounter);
  if (loadCounter <= loadBlocks)
  {
    $('#loadbar').append("<div class='load-block'></div>")
    timeout_list.push(window.setTimeout( _fillLoadbar,
                               Math.floor(Math.random()*(1.0*loadSpeed/loadBlocks)) ));
  }
  else
  {
    if (fillLoadbarCallback)
    {
      fillLoadbarCallback();
      fillLoadbarCallback = null;
      loadCounter = 0;
    }
    else
    {
      console.log("End of loading reached, but no callback set");
    }
  }
}
