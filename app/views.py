from app import app
from flask import render_template, request, jsonify

@app.route("/")
def first(methods=['GET']):
    name = request.args.get('name')
    if not name:
        return render_template("index.html")
    else:
        return render_template("gamestates/" + name + ".html")