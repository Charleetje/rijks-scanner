import fake_rpi
import sys

sys.modules['RPi'] = fake_rpi.RPi
sys.modules['RPi.GPIO'] = fake_rpi.RPi.GPIO
class spidev(object):
    class SpiDev(object):
        def open(*args, **kwargs):
            pass
        def writebytes(*args, **kwargs):
            pass
        def xfer2(*args, **kwargs):
            return [0, 0]
sys.modules['spidev'] = spidev

def get_input(number):
    return False

fake_rpi.RPi.GPIO.input = get_input

from run import *

if __name__=="__main__":
    socketio.run(app, debug=True)
