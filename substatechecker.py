
from threading import Thread, Event
from time import sleep

thread = Thread()
thread_stop_event = Event()

class SubStateCheckThread(Thread):

    def __init__(self, delay, callbacks):
        self.delay = delay
        self.callbacks = callbacks
        super(SubStateCheckThread, self).__init__()

    def remove_callback(self, cb_id):
        try:
            self.callbacks.pop(cb_id)
        except KeyError:
            print("Callback id is not registered")

    def add_callback(self, cb_id, callback):
        self.callback[cb_id] = callback

    def run(self):

        print("Checking RFID reader")
        while not thread_stop_event.isSet():

            for callback in self.callbacks.values():
                callback()

            sleep(self.delay)
